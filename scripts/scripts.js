let form1 = document.querySelector('#form1')
let form2 = document.querySelector('#form2')
let form3 = document.querySelector('#form3')

let next1 = document.querySelector('#next1')
let next2 = document.querySelector('#next2')

let back1 = document.querySelector('#back1')
let back2 = document.querySelector('#back2')

let progressBar = document.querySelector('#progress')

next1.addEventListener('click', () => {
    form1.style.left = '-450px'
    form2.style.left = '40px'
    progressBar.style.width = '240px'
})

back1.addEventListener('click', () => {
    form1.style.left = '40px'
    form2.style.left = '450px'
    progressBar.style.width = '120px'
})

next2.addEventListener('click', () => {
    form2.style.left = '-450px'
    form3.style.left = '40px'
    progressBar.style.width = '360px'
})

back2.addEventListener('click', () => {
    form2.style.left = '40px'
    form3.style.left = '450px'
    progressBar.style.width = '240px'
})